import java.util.ArrayList;

/**
 * Created by Jeremy on 6/29/2015.
 */
public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> test = new ArrayList<Integer>();
        test.add(3);
        test.add(5);
        test.add(1);
        test.add(2);
        test.add(8);
        test.add(7);
        test = mergeSort(test);
        for(int i = 0; i < test.size(); i++) {
            System.out.println(test.get(i));
        }
    }

    public static int[] mergeSort(int[] array) {
        return sort(array, 0, array.length);
    }

    public static int[] sort(int[] array, int begin, int end) {

    }

    public static int[] merge(int[] array) {

    }

    /*
     * MergeSort Algorithm for ArrayLists
     */
    public static ArrayList<Integer> mergeSort(ArrayList<Integer> array) {
        return sort(array);
    }

    /*
     * Sorting For ArrayLists
     * O(n) space
     */
    public static ArrayList<Integer> sort(ArrayList<Integer> array) {
        if(array.size() == 1) {
            return array;
        }

        int max = array.size();
        int mid = max / 2;
        ArrayList<Integer> left = new ArrayList<Integer>();
        ArrayList<Integer> right = new ArrayList<Integer>();

        for(int i = 0; i < mid; i++) {
            left.add(array.get(i));
        }
        for(int i = mid; i < max; i++) {
            right.add(array.get(i));
        }

        left = sort(left);
        right = sort(right);
        return(merge(left, right));
    }
    /*
     * Merge algorithm For ArrayLists
     * O(n) space
     */
    public static ArrayList<Integer> merge(ArrayList<Integer> left, ArrayList<Integer> right) {
        ArrayList<Integer> ret = new ArrayList<Integer>();

        int leftIndex =  0;
        int rightIndex = 0;

        while(leftIndex < left.size() && rightIndex < right.size()) {
            if (left.get(leftIndex) < right.get(rightIndex)) {
                ret.add(left.get(leftIndex));
                leftIndex++;
            } else {
                ret.add(right.get(rightIndex));
                rightIndex++;
            }
        }

        while(leftIndex < left.size()) {
            ret.add(left.get(leftIndex));
            leftIndex++;
        }
        while(rightIndex < right.size()) {
            ret.add(right.get(rightIndex));
            rightIndex++;
        }
        return ret;
    }

}
